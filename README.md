# Weather Station Research and Development Project  
  
Sources and Information for Weather Station Project  
  
Bases on and around the BinarySpace Weather Station Project - https://github.com/BinarySpace-Hackerspace/weatherstationproject  
  
Then idea to record my own notes and split out each sensors  
into their own testing app, for both the Arduino and ESP8266-12E  
  
List of environment variables to measure:  
 * Wind (speed/direction)
 * Rain (gauge/detection)
 * Temperature
 * Humidity
 * Sunlight (UV/Lux)
 * Lightning
 * Barometric pressure
 * Other sensors (self monitor [power usage]/[fan speeds]/[solar/battery charge])
  
List of sensors:  
 * DHT22 temperature/humidity sensor - https://learn.adafruit.com/dht/overview
 * TSL2561 Luminosity Sensor [Low power, digital luminosity (light) sensor] - https://learn.adafruit.com/tsl2561/overview
 * SI1145 Breakout Board - UV index / IR / Visible Sensor - https://learn.adafruit.com/adafruit-si1145-breakout-board-uv-ir-visible-sensor/overview
 * AS3935 Franklin Lightning Sensor™ - https://shop.mikroe.com/click/sensors/thunder
 * BMP280 Breakout Board  I2C or SPI Barometric Pressure & Altitude Sensor - https://learn.adafruit.com/adafruit-bmp280-barometric-pressure-plus-temperature-sensor-breakout/overview
 * INA219 I2C Current Sensor 3.2A 12BIT - https://learn.adafruit.com/adafruit-ina219-current-sensor-breakout/overview
  
Other parts and components that are need:  
 * PSU
 * Case
  
# Where to mount your Weather Station  
https://www.wunderground.com/weatherstation/installationguide.asp  
  
Wind, rain and sunlight really needs the above placement.  
  
Temperature and humidity sensor need good placement, taking into  
account anything that might influence the readings, like a shadow  
from a wall or something like that.  
  
Lightning and barometric pressure sensors are less sensitive to placement,  
 therefore almost anybody should be able to report data to places like  
 Weather Underground.